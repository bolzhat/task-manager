Задача: сделать одностраничное веб-приложение "список задач", используя AngularJS.
https://bitbucket.org/bolzhat/task-manager

Установка:
git clone https://bolzhat@bitbucket.org/bolzhat/task-manager.git
cd task-manager/
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt

Создание БД и админа:
./manage.py syncdb
По умолчанию используется SQLite 3

Дебаг:
./manage.py runserver
Работает на http://127.0.0.1:8000/
