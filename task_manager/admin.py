# -*- coding: utf-8 -*-

from django.contrib import admin
from task_manager.models import Task


class TaskAdmin(admin.ModelAdmin):
    pass

admin.site.register(Task, TaskAdmin)
