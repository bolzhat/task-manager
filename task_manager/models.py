# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Task(models.Model):
    text = models.TextField(_(u'Задача'))

    class Meta:
        verbose_name = _(u'Задача')
        verbose_name_plural = _(u'Задачи')

    def __unicode__(self):
        return self.text
