TaskApp = angular.module('TaskApp', ['ngResource', 'ngCookies'])
    .config(function($httpProvider) {
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    })
    .run(function($http, $cookies) {
        $http.defaults.headers.post['X-CSRFToken'] = $cookies.csrftoken;
        $http.defaults.xsrfCookieName = 'csrftoken';
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    });

TaskApp.factory('Task', function($resource){
    return $resource('task/', {'pk': '@pk'}, {});
});

TaskApp.controller('ListCtrl', function ($scope, Task) {
    $scope.handleError = function(error) {
        $scope.error = error.data.message;
    };
    $scope.closeAlert= function(error) {
        $scope.error = "";
    };
    
    $scope.tasks = Task.query();
    
    $scope.toggle = function(task) {
        task.changing = !task.changing;
    };
    $scope.save = function(task) {
        Task.save(
            task,
            function() {$scope.toggle(task);},
            $scope.handleError
        );
    };
    $scope.remove = function(index) {
        var task = $scope.tasks[index];
        Task.remove(
            {pk: task.pk},
            function() {$scope.tasks.splice(index, 1);},
            $scope.handleError
        )
    };
    $scope.newTask = function() {
        task = new Task({text: $scope.NewTaskText});
        task.$save(
            function() {
                $scope.NewTaskText = "";
                $scope.tasks.push(task);
            },
            $scope.handleError
        )
    };
});
