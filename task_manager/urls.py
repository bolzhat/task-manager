# -*- coding: utf-8 -*-

from django.contrib import admin
from django.conf.urls import patterns, include, url
from django.views.decorators.csrf import ensure_csrf_cookie
from task_manager.views import TaskCRUDView

admin.autodiscover()

urlpatterns = patterns('task_manager.views',
    url(r'^$', 'home', name='home'),
    url(r'^task/?$', ensure_csrf_cookie(TaskCRUDView.as_view()), name='task_crud'),
    url(r'^admin/', include(admin.site.urls)),
)
