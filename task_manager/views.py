# -*- coding: utf-8 -*-

from django.shortcuts import render
from djangular.views.crud import NgCRUDView
from task_manager.models import Task


def home(request):
    return render(request, 'task_manager/home.html')


class TaskCRUDView(NgCRUDView):
    model = Task
